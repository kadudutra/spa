<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LinguagensTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LinguagensTable Test Case
 */
class LinguagensTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LinguagensTable
     */
    public $Linguagens;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Linguagens'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Linguagens') ? [] : ['className' => LinguagensTable::class];
        $this->Linguagens = TableRegistry::getTableLocator()->get('Linguagens', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Linguagens);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

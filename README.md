# CakePHP SPA

## Installation

1. Clone o repositório `https://gitlab.com/kadudutra/spa.git`
2. Abra o terminal, e dentro da pasta do projeto execute o comando `docker-compose up -d`
3. após execute `docker exec -it spa_php_1 bash`
4. instale as dependencias `composer update`
5. as pastas `tmp`, `logs`, `bin/cake` e `config/Migrations` precisam de permissão 777
6. execute o script de criação das tabelas `bin/cake migrations migrate`

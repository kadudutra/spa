$('input[name="nome"]').on('keyup', function () {
    let text = $(this).val();
    searchProjeto(text);
});

function searchProjeto(text) {
    $.ajax({
        cache: false,
        method: 'GET',
        url: site + '/projetos/search',
        data: {q: text},
        success: function (data) {
            $('#content').html(data);
        }
    });
}

$('#addProjeto').on('click', function () {
    $.ajax({
        cache: false,
        method: 'GET',
        url: site + '/projetos/add',
        success: function (data) {
            $('#main-content').fadeIn().html(data);
        }
    })
});

$('.edit').on('click', function () {
    const id = $(this).closest('td').data('id');
    $.ajax({
        cache: false,
        method: 'GET',
        url: site + '/projetos/edit/' + id,
        success: function (data) {
            $('#main-content').fadeIn().html(data);
        }
    });
});


$('.view').on('click', function () {
    const id = $(this).closest('td').data('id');
    $.ajax({
        cache: false,
        method: 'GET',
        url: site + '/projetos/view/' + id,
        success: function (data) {
            $('#main-content').fadeIn().html(data);
        }
    });
});

$('.del').on('click', function () {
    const id = $(this).closest('td').data('id');
    $.ajax({
        cache: false,
        method: 'GET',
        url: site + '/projetos/delete/' + id,
        success: function (data) {
            listaProjetos();
        }
    });
});

$('input[name="nome"]').on('keyup', function () {
    let text = $(this).val();
    searchLinguagem(text);
});

function searchLinguagem(text) {
    $.ajax({
        cache: false,
        method: 'GET',
        url: site + '/linguagens/search',
        data: {q: text},
        success: function (data) {
            $('#content').html(data);
        }
    });
}

$('#addLinguagem').on('click', function () {
    $.ajax({
        cache: false,
        method: 'GET',
        url: site + '/linguagens/add',
        success: function (data) {
            $('#main-content').fadeIn().html(data);
        }
    })
});

$('.edit').on('click', function () {
    const id = $(this).closest('td').data('id');
    $.ajax({
        cache: false,
        method: 'GET',
        url: site + '/linguagens/edit/' + id,
        success: function (data) {
            $('#main-content').fadeIn().html(data);
        }
    });
});

$('.view').on('click', function () {
    const id = $(this).closest('td').data('id');
    $.ajax({
        cache: false,
        method: 'GET',
        url: site + '/linguagens/view/' + id,
        success: function (data) {
            $('#main-content').fadeIn().html(data);
        }
    });
});

$('.del').on('click', function () {
    const id = $(this).closest('td').data('id');
    $.ajax({
        cache: false,
        method: 'GET',
        url: site + '/linguagens/delete/' + id,
        success: function (data) {
            listaLinguagens();
        }
    });
});

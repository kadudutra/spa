$('input[name="nome"]').on('keyup', function () {
    searchFuncionario();
});

$('input[name="idade"]').on('keyup', function () {
    searchFuncionario();
});

$('select[name="sexo"]').on('change', function () {
    searchFuncionario();
});

function searchFuncionario() {
    let nome = $('input[name="nome"]').val();
    let idade = $('input[name="idade"]').val();
    let sexo = $('select[name="sexo"]').val();
    $.ajax({
        cache: false,
        method: 'GET',
        url: site + '/funcionarios/search',
        data: {
            nome: nome,
            idade: idade,
            sexo: sexo,
        },
        success: function (data) {
            $('#content').html(data);
        }
    });
}

$('#addFuncionario').on('click', function () {
    $.ajax({
        cache: false,
        method: 'GET',
        url: site + '/funcionarios/add',
        success: function (data) {
            $('#main-content').fadeIn().html(data);
        }
    })
});

$('.edit').on('click', function () {
    const id = $(this).closest('td').data('id');
    $.ajax({
        cache: false,
        method: 'GET',
        url: site + '/funcionarios/edit/' + id,
        success: function (data) {
            $('#main-content').fadeIn().html(data);
        }
    });
});

$('.view').on('click', function () {
    const id = $(this).closest('td').data('id');
    $.ajax({
        cache: false,
        method: 'GET',
        url: site + '/funcionarios/view/' + id,
        success: function (data) {
            $('#main-content').fadeIn().html(data);
        }
    });
});

$('.del').on('click', function () {
    const id = $(this).closest('td').data('id');
    console.log(id);
    $.ajax({
        cache: false,
        method: 'GET',
        url: site + '/funcionarios/delete/' + id,
        success: function (data) {
            listaFuncionarios();
        }
    });
});

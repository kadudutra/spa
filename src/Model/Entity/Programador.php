<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Programadore Entity
 *
 * @property int $id
 * @property int $funcionario_id
 * @property int $linguagem_id
 *
 * @property \App\Model\Entity\Funcionario $funcionario
 * @property \App\Model\Entity\Linguagem $linguagem
 */
class Programador extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'funcionario_id' => true,
        'linguagem_id' => true,
        'funcionario' => true,
        'linguagem' => true,
    ];
}

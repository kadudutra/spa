<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Projeto[]|\Cake\Collection\CollectionInterface $projetos
 */
?>

<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">
                <?php echo __('Filtros'); ?>
            </h3>
        </div>
        <?php echo $this->Form->create(null, ['valueSources' => 'query']); ?>
        <div class="box-body">
            <?php echo $this->Form->control('nome'); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">
                <?php echo __('Projetos'); ?>
            </h3>
            <div class="btn-toolbar pull-right box-tools">
                <div class="btn-group">
                    <a href="#" id="addProjeto" class="btn btn-sm btn-success"
                       title="<?php echo __('Adicionar'); ?>">
                        <i class="fa fa-plus"></i>
                        <?php echo __('Adicionar'); ?>
                    </a>
                </div>
            </div>
        </div>
        <div id="content">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Criado em</th>
                                    <th scope="col">Modificado em</th>
                                    <th scope="col" class="actions"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($projetos)) { ?>
                                    <?php foreach ($projetos as $projeto) { ?>
                                        <tr>
                                            <td class="text-center"><?php echo $this->Number->format($projeto->id) ?></td>
                                            <td><?php echo $projeto->nome ?></td>
                                            <td><?php echo h($projeto->created->nice('America/Sao_Paulo', 'pt_BR')) ?></td>
                                            <td><?php echo h($projeto->modified->nice('America/Sao_Paulo', 'pt_BR')) ?></td>
                                            <td class="actions"
                                                data-id="<?php echo $this->Number->format($projeto->id) ?>">
                                                <a href="#" class="btn btn-primary view" title="Visualizar">
                                                    <i class="fa fa-search"></i>
                                                </a>
                                                <a href="#" class="btn btn-success edit" title="Editar">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="#" class="btn btn-danger del" title="Deletar">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="5">Nenhum registro encontrado.</td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer clear">
                <p><?php echo $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, exibindo {{current}} registro(s) de um total de {{count}}.')]) ?></p>
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php echo $this->Paginator->first(__('Primeiro')) ?>
                    <?php echo $this->Paginator->prev('< ' . __('Anterior')) ?>
                    <?php echo $this->Paginator->numbers() ?>
                    <?php echo $this->Paginator->next(__('Próximo') . ' >') ?>
                    <?php echo $this->Paginator->last(__('Último')) ?>
                </ul>
            </div>
        </div>
    </div>
</section>

<?php echo $this->Html->script('projeto'); ?>

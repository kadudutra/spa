<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Projeto $projeto
 */
?>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <tr>
                    <th scope="row"><?= __('ID') ?></th>
                    <td><?= $this->Number->format($projeto->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Nome') ?></th>
                    <td><?= h($projeto->nome) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Criado em') ?></th>
                    <td><?= h($projeto->created->nice('America/Sao_Paulo', 'pt_BR')) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Modificado em') ?></th>
                    <td><?= h($projeto->modified->nice('America/Sao_Paulo', 'pt_BR')) ?></td>
                </tr>
            </table>
        </div>
    </div>
</section>

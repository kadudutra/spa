<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Projeto $projeto
 */
?>

<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">
                <?php echo __('Editar Projeto') ?>
            </h3>
        </div>
        <?php echo $this->Form->create($projeto); ?>
        <div class="box-body">
            <?php
            echo $this->Form->control('id', [
                'type' => 'hidden',
            ]);
            echo $this->Form->control('nome');
            ?>
        </div>
        <div class="box-footer">
            <div class="pull-right">
                <div class="btn-toolbar">
                    <a id="save" class="btn btn-success" title="Salvar"><i class="fa fa-save"></i>
                        Salvar
                    </a>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</section>

<script>

    $("form").submit(function (e) {
        $('#save').trigger('click');
        e.preventDefault();
    });

    $('#save').on('click', function (e) {
        const name = $('input[name="nome"]');
        const id = $('input[name="id"]').val();
        if (name.val().length > 0) {
            $.ajax({
                cache: false,
                method: 'POST',
                url: site + '/projetos/edit/' + id,
                data: {
                    nome: name.val(),
                    _csrfToken: $('input[name="_csrfToken"]').val(),
                },
                success: function () {
                    listaProjetos();
                }
            });
        } else {
            name.append('<p>O nome é obrigatório</p>');
        }

        e.preventDefault();
    })
</script>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Funcionario[]|\Cake\Collection\CollectionInterface $funcionarios
 */
?>

<div class="box-body">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Sexo</th>
                        <th scope="col">Idade</th>
                        <th scope="col">Profissão</th>
                        <th scope="col">Projeto/Linguagem</th>
                        <th scope="col">Criado em</th>
                        <th scope="col">Modificado em</th>
                        <th scope="col" class="actions"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($funcionarios)) { ?>
                        <?php foreach ($funcionarios as $funcionario) { ?>
                            <tr>
                                <td class="text-center"><?php echo $this->Number->format($funcionario->id) ?></td>
                                <td><?php echo $funcionario->nome ?></td>
                                <td><?php echo $sexos[$funcionario->sexo] ?></td>
                                <td><?php echo $funcionario->idade ?></td>
                                <td><?php echo (isset($funcionario->analistas[0])) ? 'Analista' : 'Programador' ?></td>
                                <td><?php echo ($funcionario->analistas[0]->projeto->nome) ?? $funcionario->programadores[0]->linguagen->nome ?></td>
                                <td><?php echo h($funcionario->created->nice('America/Sao_Paulo', 'pt_BR')) ?></td>
                                <td><?php echo h($funcionario->modified->nice('America/Sao_Paulo', 'pt_BR')) ?></td>
                                <td class="actions"
                                    data-id="<?php echo $this->Number->format($funcionario->id) ?>">
                                    <a href="#" class="btn btn-primary view" title="Visualizar">
                                        <i class="fa fa-search"></i>
                                    </a>
                                    <a href="#" class="btn btn-success edit" title="Editar">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a href="#" class="btn btn-danger del" title="Deletar">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } else { ?>
                        <tr>
                            <td colspan="9">Nenhum registro encontrado.</td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Funcionario $funcionario
 */
?>

<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">
                <?= __('Adicionar Funcionário') ?>
            </h3>
        </div>
        <?php echo $this->Form->create($funcionario); ?>
        <div class="box-body">
            <?php
            echo $this->Form->control('tipo', [
                'empty' => 'Selecione',
                'options' => $tipos,
            ]);
            echo $this->Form->control('nome');
            echo $this->Form->control('sexo', [
                'options' => $sexos,
            ]);
            echo $this->Form->control('idade');
            echo $this->Form->control('analistas.0.projeto_id', [
                'empty' => 'Selecione',
                'options' => $projetos,
            ]);
            echo $this->Form->control('programadores.0.linguagem_id', [
                'empty' => 'Selecione',
                'options' => $linguagens,
            ]);
            ?>
        </div>
        <div class="box-footer">
            <div class="pull-right">
                <div class="btn-toolbar">
                    <a id="save" class="btn btn-success" title="Salvar"><i class="fa fa-save"></i>
                        Salvar
                    </a>
                </div>
            </div>
        </div>
        <?php $this->Form->end() ?>
    </div>
</section>

<script>
    $('#save').on('click', function (e) {
        const tipo = $('select[name="tipo"]').val();
        const name = $('input[name="nome"]');
        const sexo = $('select[name="sexo"]').val();
        const idade = $('input[name="idade"]').val();
        const projeto_id = $('select[name="analistas[0][projeto_id]"]').val();
        const linguagem_id = $('select[name="programadores[0][linguagem_id]"]').val();

        if (name.val().length > 0) {
            $.ajax({
                cache: false,
                method: 'POST',
                url: site + '/funcionarios/add',
                data: {
                    tipo: tipo,
                    nome: name.val(),
                    sexo: sexo,
                    idade: idade,
                    projeto_id: projeto_id,
                    linguagem_id: linguagem_id,
                    _csrfToken: $('input[name="_csrfToken"]').val(),
                },
                success: function () {
                    listaFuncionarios();
                }
            });
        } else {
            name.append('<p>O nome é obrigatório</p>');
        }

        e.preventDefault();
    });

    $('#tipo').on('change', function () {
        const val = $(this).val();
        if (val == 'a') {
            $('#analistas-0-projeto-id').closest('.form-group').show();
            $('#programadores-0-linguagem-id').closest('.form-group').hide();
        } else if (val == 'p') {
            $('#analistas-0-projeto-id').closest('.form-group').hide();
            $('#programadores-0-linguagem-id').closest('.form-group').show();
        } else {
            $('#analistas-0-projeto-id').closest('.form-group').hide();
            $('#programadores-0-linguagem-id').closest('.form-group').hide();
        }
    });

    $(document).ready(function () {
        $('#tipo').trigger('change');
    })
</script>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Funcionario $funcionario
 */
?>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <tr>
                    <th scope="row"><?= __('ID') ?></th>
                    <td><?= $this->Number->format($funcionario->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Nome') ?></th>
                    <td><?= h($funcionario->nome) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sexo') ?></th>
                    <td><?= h($sexos[$funcionario->sexo]) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Idade') ?></th>
                    <td><?= h($funcionario->idade) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Profissão') ?></th>
                    <td><?php echo (isset($funcionario->analistas[0])) ? 'Analista' : 'Programador' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Linguagem/Projeto') ?></th>
                    <td><?php echo ($funcionario->analistas[0]->projeto->nome) ?? $funcionario->programadores[0]->linguagen->nome ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Criado em') ?></th>
                    <td><?= h($funcionario->created->nice('America/Sao_Paulo', 'pt_BR')) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Modificado em') ?></th>
                    <td><?= h($funcionario->modified->nice('America/Sao_Paulo', 'pt_BR')) ?></td>
                </tr>
            </table>
        </div>
    </div>
</section>

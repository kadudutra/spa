<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<div id="main-content"></div>

<script>
    $(document).ready(function () {
    });

    const site = location.origin;

    $('#projetos').on('click', function () {
        listaProjetos();
    });

    function listaProjetos() {
        $.ajax({
            cache: false,
            method: 'GET',
            url: site + '/projetos/index',
            success: function (data) {
                $('#main-content').html(data);
            }
        });
    }

    $('#linguagens').on('click', function () {
        listaLinguagens();
    });

    function listaLinguagens() {
        $.ajax({
            cache: false,
            method: 'GET',
            url: site + '/linguagens/index',
            success: function (data) {
                $('#main-content').html(data);
            }
        });
    }

    $('#funcionarios').on('click', function () {
        listaFuncionarios();
    });

    function listaFuncionarios() {
        $.ajax({
            cache: false,
            method: 'GET',
            url: site + '/funcionarios/index',
            success: function (data) {
                $('#main-content').fadeIn().html(data);
            }
        })
    }

    $('#addFuncionario').on('click', function () {
        $.ajax({
            cache: false,
            method: 'GET',
            url: site + '/funcionarios/add',
            success: function (data) {
                $('#main-content').fadeIn().html(data);
            }
        })
    });


    $('#addLinguagem').on('click', function () {
        $.ajax({
            cache: false,
            method: 'GET',
            url: site + '/linguagens/add',
            success: function (data) {
                $('#main-content').fadeIn().html(data);
            }
        })
    });


    $("form").submit(function (e) {
        $('#save').trigger('click');
        e.preventDefault();
    });
</script>

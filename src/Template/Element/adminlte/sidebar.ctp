<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
        <li class="header">Navegação</li>
        <!-- Optionally, you can add icons to the links -->
        <li>
            <a href="#" id="projetos">
                <i class="fa fa-link"></i> <span>Projetos</span>
            </a>
        </li>
        <li>
            <a href="#" id="linguagens">
                <i class="fa fa-link"></i> <span>Linguagens</span>
            </a>
        </li>
        <li>
            <a href="#" id="funcionarios">
                <i class="fa fa-link"></i> <span>Funcionários</span>
            </a>
        </li>
    </ul>
    <!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Analista $analista
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Edit Analista'), ['action' => 'edit', $analista->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Analista'), ['action' => 'delete', $analista->id], ['confirm' => __('Are you sure you want to delete # {0}?', $analista->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Analistas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Analista'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Funcionarios'), ['controller' => 'Funcionarios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Funcionário'), ['controller' => 'Funcionarios', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Projetos'), ['controller' => 'Projetos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Projeto'), ['controller' => 'Projetos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="analistas view large-9 medium-8 columns content">
    <h3><?= h($analista->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Funcionário') ?></th>
            <td><?= $analista->has('funcionario') ? $this->Html->link($analista->funcionario->id, ['controller' => 'Funcionarios', 'action' => 'view', $analista->funcionario->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Projeto') ?></th>
            <td><?= $analista->has('projeto') ? $this->Html->link($analista->projeto->nome, ['controller' => 'Projetos', 'action' => 'view', $analista->projeto->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($analista->id) ?></td>
        </tr>
    </table>
</div>

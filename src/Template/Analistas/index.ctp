<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Analista[]|\Cake\Collection\CollectionInterface $analistas
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('New Analista'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Funcionarios'), ['controller' => 'Funcionarios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Funcionário'), ['controller' => 'Funcionarios', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Projetos'), ['controller' => 'Projetos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Projeto'), ['controller' => 'Projetos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="analistas index large-9 medium-8 columns content">
    <h3><?= __('Analistas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('funcionario_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('projeto_id') ?></th>
            <th scope="col" class="actions"><?= __('Ações') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($analistas as $analista): ?>
            <tr>
                <td><?= $this->Number->format($analista->id) ?></td>
                <td><?= $analista->has('funcionario') ? $this->Html->link($analista->funcionario->id, ['controller' => 'Funcionarios', 'action' => 'view', $analista->funcionario->id]) : '' ?></td>
                <td><?= $analista->has('projeto') ? $this->Html->link($analista->projeto->nome, ['controller' => 'Projetos', 'action' => 'view', $analista->projeto->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $analista->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $analista->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $analista->id], ['confirm' => __('Are you sure you want to delete # {0}?', $analista->id)]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Analista $analista
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('List Analistas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Funcionarios'), ['controller' => 'Funcionarios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Funcionário'), ['controller' => 'Funcionarios', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Projetos'), ['controller' => 'Projetos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Projeto'), ['controller' => 'Projetos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="analistas form large-9 medium-8 columns content">
    <?= $this->Form->create($analista) ?>
    <fieldset>
        <legend><?= __('Adicionar Analista') ?></legend>
        <?php
        echo $this->Form->control('funcionario_id', ['options' => $funcionarios]);
        echo $this->Form->control('projeto_id', ['options' => $projetos]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Salvar')) ?>
    <?= $this->Form->end() ?>
</div>

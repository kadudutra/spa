<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Programador $programador
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Edit Programadore'), ['action' => 'edit', $programador->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Programadore'), ['action' => 'delete', $programador->id], ['confirm' => __('Are you sure you want to delete # {0}?', $programador->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Programadores'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Programadore'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Funcionarios'), ['controller' => 'Funcionarios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Funcionário'), ['controller' => 'Funcionarios', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Linguagens'), ['controller' => 'Linguagens', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Linguagen'), ['controller' => 'Linguagens', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="programadores view large-9 medium-8 columns content">
    <h3><?= h($programador->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Funcionário') ?></th>
            <td><?= $programador->has('funcionario') ? $this->Html->link($programador->funcionario->id, ['controller' => 'Funcionarios', 'action' => 'view', $programador->funcionario->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Linguagen') ?></th>
            <td><?= $programador->has('linguagem') ? $this->Html->link($programador->linguagem->id, ['controller' => 'Linguagens', 'action' => 'view', $programador->linguagem->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($programador->id) ?></td>
        </tr>
    </table>
</div>

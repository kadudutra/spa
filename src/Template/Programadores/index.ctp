<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Programador[]|\Cake\Collection\CollectionInterface $programadores
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('New Programadore'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Funcionarios'), ['controller' => 'Funcionarios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Funcionário'), ['controller' => 'Funcionarios', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Linguagens'), ['controller' => 'Linguagens', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Linguagen'), ['controller' => 'Linguagens', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="programadores index large-9 medium-8 columns content">
    <h3><?= __('Programadores') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('funcionario_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('linguagem_id') ?></th>
            <th scope="col" class="actions"><?= __('Ações') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($programadores as $programador): ?>
            <tr>
                <td><?= $this->Number->format($programador->id) ?></td>
                <td><?= $programador->has('funcionario') ? $this->Html->link($programador->funcionario->id, ['controller' => 'Funcionarios', 'action' => 'view', $programador->funcionario->id]) : '' ?></td>
                <td><?= $programador->has('linguagen') ? $this->Html->link($programador->linguagen->id, ['controller' => 'Linguagens', 'action' => 'view', $programador->linguagen->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $programador->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $programador->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $programador->id], ['confirm' => __('Are you sure you want to delete # {0}?', $programador->id)]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

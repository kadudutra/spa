<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Programador $programador
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('List Programadores'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Funcionarios'), ['controller' => 'Funcionarios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Funcionário'), ['controller' => 'Funcionarios', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Linguagens'), ['controller' => 'Linguagens', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Linguagen'), ['controller' => 'Linguagens', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="programadores form large-9 medium-8 columns content">
    <?= $this->Form->create($programador) ?>
    <fieldset>
        <legend><?= __('Adicionar Programadore') ?></legend>
        <?php
        echo $this->Form->control('funcionario_id', ['options' => $funcionarios]);
        echo $this->Form->control('linguagem_id', ['options' => $linguagens]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Salvar')) ?>
    <?= $this->Form->end() ?>
</div>

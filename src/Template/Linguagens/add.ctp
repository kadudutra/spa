<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Projeto $linguagem
 */
?>

<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">
                <?php echo __('Adicionar Linguagem') ?>
            </h3>
        </div>
        <?php echo $this->Form->create($linguagem); ?>
        <div class="box-body">
            <?php
            echo $this->Form->control('nome');
            ?>
        </div>
        <div class="box-footer">
            <div class="pull-right">
                <div class="btn-toolbar">
                    <a id="save" class="btn btn-success" title="Salvar"><i class="fa fa-save"></i>
                        Salvar
                    </a>
                </div>
            </div>
        </div>
        <?php $this->Form->end() ?>
    </div>
</section>

<script>
    $('#save').on('click', function (e) {
        const name = $('input[name="nome"]');
        if (name.val().length > 0) {
            $.ajax({
                cache: false,
                method: 'POST',
                url: site + '/linguagens/add',
                data: {
                    nome: name.val(),
                    _csrfToken: $('input[name="_csrfToken"]').val(),
                },
                success: function () {
                    listaLinguagens();
                }
            });
        } else {
            name.append('<p>O nome é obrigatório</p>');
        }

        e.preventDefault();
    });
</script>

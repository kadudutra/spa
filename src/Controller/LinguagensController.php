<?php

namespace App\Controller;

use Cake\Database\Expression\QueryExpression;

/**
 * Linguagens Controller
 *
 * @property \App\Model\Table\LinguagensTable $Linguagens
 *
 * @method \App\Model\Entity\Linguagem[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LinguagensController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $linguagens = $this->paginate($this->Linguagens);

        $this->set(compact('linguagens'));
    }

    /**
     * View method
     *
     * @param string|null $id Linguagem id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $linguagem = $this->Linguagens->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('linguagem'));
    }

    /**
     * Add method
     *
     */
    public function add()
    {
        $linguagem = $this->Linguagens->newEntity();
        if ($this->request->is('post')) {
            $linguagem = $this->Linguagens->patchEntity($linguagem, $this->request->getData());
            if ($this->Linguagens->save($linguagem)) {
                $this->Flash->success(__('O  linguagen foi salvo com sucesso.'));
            }
            $this->Flash->error(__('O  linguagen não pode ser salvo. Por favor, tente novamente.'));
        }
        $this->set(compact('linguagem'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Linguagem id.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $linguagem = $this->Linguagens->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['post'])) {
            $linguagem = $this->Linguagens->patchEntity($linguagem, $this->request->getData());
            if ($this->Linguagens->save($linguagem)) {
                $this->Flash->success(__('O  linguagen foi salvo com sucesso.'));
            }
            $this->Flash->error(__('O  linguagen não pode ser salvo. Por favor, tente novamente.'));
        }
        $this->set(compact('linguagem'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Linguagem id.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['get']);
        $linguagem = $this->Linguagens->get($id);
        if ($this->Linguagens->delete($linguagem)) {
            $this->Flash->success(__('O  linguagen foi excluido.'));
        } else {
            $this->Flash->error(__('O  linguagen não pode ser excluido. Por favor, tente novamente.'));
        }
    }

    /**
     * search
     */
    public function search()
    {
        $this->request->allowMethod(['ajax']);
        $text = $this->request->getQuery('q');

        $query = $this->Linguagens->find()
            ->where(function (QueryExpression $expression) use ($text) {
                return $expression->like('nome', '%' . $text . '%');
            })
            ->order([
                'nome' => 'ASC',
            ]);

        $linguagens = $this->paginate($query);

        $this->set(compact('linguagens'));
    }
}

<?php

namespace App\Controller;

use Cake\Database\Expression\QueryExpression;

/**
 * Projetos Controller
 *
 * @property \App\Model\Table\ProjetosTable $Projetos
 *
 * @method \App\Model\Entity\Projeto[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProjetosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $projetos = $this->paginate($this->Projetos);

        $this->set(compact('projetos'));
    }

    /**
     * View method
     *
     * @param string|null $id Projeto id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $projeto = $this->Projetos->get($id, [
            'contain' => ['Analistas'],
        ]);

        $this->set('projeto', $projeto);
    }

    /**
     * Add method
     */
    public function add()
    {
        $projeto = $this->Projetos->newEntity();
        if ($this->request->is(['post'])) {
            $projeto = $this->Projetos->patchEntity($projeto, $this->request->getData());
            if ($this->Projetos->save($projeto)) {
                $this->Flash->success(__('O Projeto foi salvo com sucesso.'));
            }
            $this->Flash->error(__('O  projeto não pode ser salvo. Por favor, tente novamente.'));
        }
        $this->set(compact('projeto'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Projeto id.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $projeto = $this->Projetos->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['post'])) {
            $projeto = $this->Projetos->patchEntity($projeto, $this->request->getData());
            if ($this->Projetos->save($projeto)) {
                $this->Flash->success(__('O Projeto foi salvo com sucesso.'));
            }
            $this->Flash->error(__('O Projeto não pode ser salvo. Por favor, tente novamente.'));
        }
        $this->set(compact('projeto'));
    }

    /**
     * Delete method
     *
     * @param null $id
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['get']);
        $projeto = $this->Projetos->get($id);
        if ($this->Projetos->delete($projeto)) {
            $this->Flash->success(__('O projeto foi deletado.'));
        } else {
            $this->Flash->error(__('O  projeto não pode ser excluido. Por favor, tente novamente.'));
        }
    }

    /**
     * search
     */
    public function search()
    {
        $this->request->allowMethod(['ajax']);
        $text = $this->request->getQuery('q');

        $query = $this->Projetos->find()
            ->where(function (QueryExpression $expression) use ($text) {
                return $expression->like('nome', '%' . $text . '%');
            })
            ->order([
                'nome' => 'ASC',
            ]);

        $projetos = $this->paginate($query);

        $this->set(compact('projetos'));
    }
}

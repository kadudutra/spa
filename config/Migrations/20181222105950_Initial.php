<?php
use Migrations\AbstractMigration;

class Initial extends AbstractMigration
{
    public function up()
    {

        $this->table('analistas')
            ->addColumn('funcionario_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('projeto_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'funcionario_id',
                ]
            )
            ->addIndex(
                [
                    'projeto_id',
                ]
            )
            ->create();

        $this->table('funcionarios')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('sexo', 'string', [
                'default' => null,
                'limit' => 1,
                'null' => true,
            ])
            ->addColumn('idade', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('linguagens')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('programadores')
            ->addColumn('funcionario_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('linguagem_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'funcionario_id',
                ]
            )
            ->addIndex(
                [
                    'linguagem_id',
                ]
            )
            ->create();

        $this->table('projetos')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('analistas')
            ->addForeignKey(
                'funcionario_id',
                'funcionarios',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'projeto_id',
                'projetos',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('programadores')
            ->addForeignKey(
                'funcionario_id',
                'funcionarios',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'linguagem_id',
                'linguagens',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();
    }

    public function down()
    {
        $this->table('analistas')
            ->dropForeignKey(
                'funcionario_id'
            )
            ->dropForeignKey(
                'projeto_id'
            )->save();

        $this->table('programadores')
            ->dropForeignKey(
                'funcionario_id'
            )
            ->dropForeignKey(
                'linguagem_id'
            )->save();

        $this->table('analistas')->drop()->save();
        $this->table('funcionarios')->drop()->save();
        $this->table('linguagens')->drop()->save();
        $this->table('programadores')->drop()->save();
        $this->table('projetos')->drop()->save();
    }
}
